# iframing-bb

Scripts en handleiding (deze README) om de Berkeley Bridge presentatielaag te i-framen in eigen site.

Vervang hieronder telkens 'bb.your-site.nl' met de plek waar de Berkeley Bridge presentatielaag daadwerkelijk draait.

# Sitebeheerders en web developer Berkeley Bridge stemmen de te gebruiken URLs af

1. child (iframe): URL waar Berkeley Bridge presentatielaag zelf draait, bv. `https://DOMEIN_VAN_IFRAME`
2. parent: URL waar Berkeley Bridge presentatielaag ge-iframed wordt, bv. `https://www.your-site.nl/balie`

Hieronder staan onder Parent de acties die de sitebeheerder moet ondernemen, en onder child de acties die de web developers van Berkeley Bridge moeten ondernemen.

# Child

## Pas configuratie van de webomgeving (`config.json`) aan:

    {
      ....
      "plugins": [
        ...
        "shout-out-resize",
        ...
      ]
      ....
      "arbitrary": {
        "parentDomain" : 'https://parent.your-site.nl'
       }
     }

Bak de omgeving en zet hem neer (als gehost bij Berkeley Bridge)/lever hem aan applicatiebeheerder.

# Parent

## Zorg ervoor dat de CSP header goed staat

Bijvoorbeeld:

    Content-Security-Policy: 'self' https://DOMEIN_VAN_IFRAME

## Neem de volgende iframe-code op op de site

    <iframe id="bb" src="https://DOMEIN_VAN_IFRAME" style="height: 3700px;">
      <a href="https://DOMEIN_VAN_IFRAME">open de Berkeley Bridge presentatielaag</a>
    </iframe>`

Het id (`bb`) zorgt ervoor dat het resize script het `iframe` vinden kan.

Zorg ervoor de Berkeley Bridge presentatielaag ook te ontsluiten als `iframes` geblokkeerd of niet ondersteund worden door een link (`<a>`)
op te nemen naar dezelfde content ([Voldoen aan succescriterium U.7.1 middels afdoende techniek](http://versie2.webrichtlijnen.nl/techniek/Hu4/)).
Dit kan zowel binnen als buiten het iframe.

**NB** Pas `src` en `href` aan naar het afgestemde child url.

## Neem een resize script op op de pagina waar de Berkeley Bridge presentatielaag ge-iframed wordt

Het `iframe` zal elke keer dat het een andere hoogte nodig heeft een `message` sturen naar de pagina met het `iframe`.

In `resize-scripts` zijn scripts te vinden die reageren op het `message` ervoor zorgen dat deze ruimte aan het `iframe` gegeven wordt.

### resize-scripts/resize-same-domain.js

Gebruik deze als de pagina met het `iframe` en de Berkeley Bridge presentatielaag allebei op een subdomein draaien van hetzelfde domein en allebei onder hetzelfde protocol draaien (HTTPS).

### resize-scripts/resize-one-domain.js

Gebruik dit script als de pagina met het `iframe` en de Berkeley Bridge presentatielaag _niet_ op een subdomein draaien van hetzelfde domein of als je de communicatie verder wilt beperken.

**NB** Dit script moet wel eerst aangepast worden (de waarde van `origin` op regel 3) om hardcoded de origin te checken.

### resize-scripts/resize-all-domains.js

Dit script voert _geen check_ uit op de origin van het message.

Gebruik dit script om te testen.
